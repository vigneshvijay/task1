import React, { useState } from 'react';
import swal from 'sweetalert';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Row, Col, Card, Button, Form } from 'react-bootstrap';
import TableCustom from './TableCustom';
function Home() {
    const yearCheck = '^[12][0-9]{3}$';
    const [year, setYear] = useState('');
    const [data, setData] = useState([]);
    const handleSubmit = (event) => {
        event.preventDefault();
        if (!year.match(yearCheck)) {
            swal('', 'Please enter a valid Year in YYYY format', 'warning');
        }
        const dataFromApi = async () => {
            const response = await fetch(
                `https://jsonmock.hackerrank.com/api/movies?Year=${year}`,
                { method: 'GET' }
            );
            const json = await response.json();
            setData(json.data);
        };
        dataFromApi();
    };
    return (
        <div>
            <Row>
                <Col md={12} xl={12}>
                    <Card className='px-5 py-5'>
                        <Form>
                            <Form.Group>
                                <Form.Label>Please provide an year to get started</Form.Label>
                                <br />
                                <Form.Control
                                    value={year}
                                    onChange={(e) => setYear(e.target.value)}
                                    type='lg'
                                    placeholder='year in YYYY format'
                                />
                                <br />
                                <Button onClick={handleSubmit}>Submit</Button>
                            </Form.Group>
                        </Form>
                        <TableCustom data={data} />
                    </Card>
                </Col>
            </Row>
        </div>
    );
}

export default Home;